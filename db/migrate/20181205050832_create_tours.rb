class CreateTours < ActiveRecord::Migration
  def change
    create_table :tours do |t|
      t.string :title
      t.string :area
      t.string :address
      t.text :url
      t.text :text
      t.integer :user_id
      t.integer :impressions_count, default: 0

      t.timestamps null: false
    end
  end
end
