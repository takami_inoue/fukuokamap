class User < ActiveRecord::Base
  has_many :tours, dependent: :destroy
end
