class Image < ActiveRecord::Base
  belongs_to :tour
  mount_uploader :img, ImageUploader
  # serialize :img, JSON
end
