class Tour < ActiveRecord::Base
  belongs_to :user
  has_many :images, dependent: :destroy
  accepts_nested_attributes_for :images #アトリビュート設定
  
  is_impressionable counter_cache: true
  
  geocoded_by :address
  after_validation :geocode
end
