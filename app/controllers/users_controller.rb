class UsersController < ApplicationController
  def index
    @user = User.find_by(uid: session[:login_uid]).id
    @tours = Tour.where(user_id: @user).page(params[:page]).per(10).order(:id)
  end

  def new
    @user = User.new
  end
  def create
    @user = User.new(
      uid: params[:user][:uid],
      password: BCrypt::Password.create(params[:user][:password]))
      @user.save
      redirect_to login_path
  end
  
  def destroy
    User.find(params[:id]).destroy
    redirect_to users_path
  end
end
