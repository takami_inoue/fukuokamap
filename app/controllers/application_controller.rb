class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  helper_method :login_check
  
  private
  def current_user
    if session[:login_uid]
      User.find_by(uid: session[:login_uid])
    end
  end
end
