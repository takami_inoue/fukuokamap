class TopsController < ApplicationController
  def index
    @users = User.all
    @rank = Tour.order('impressions_count DESC').take(3)
  end
  
  def login_form
  end
  
  def login
    user = User.find_by(uid: params[:uid])
    if user && BCrypt::Password.new(user.password) == params[:password]
      session[:login_uid] = user[:uid]
      redirect_to root_path
    else
      render 'tops/login'
    end
  end

  def logout
    session.delete(:login_uid)
    redirect_to root_path
  end
end
