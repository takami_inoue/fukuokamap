class ToursController < ApplicationController
  impressionist :actions=> [:show]
  
  def index
    @tours = Tour.page(params[:page]).per(10).order(:id)
  end

  def show
    @tour = Tour.find(params[:id])
    @image = Image.where(tour_id: params[:id])
    impressionist(@tour, nil, unique: [:session_hash])
  end

  def new
    @tour = Tour.new
    @image = Image.new
    3.times { @tour.images.build }
  end
  def create
    @tour = Tour.create(create_params)
    @tour.user_id = User.find_by(uid: session[:login_uid]).id
    @tour.save
    
    redirect_to tour_path(@tour)
  end

  def edit
    @tour = Tour.find(params[:id])
  end
  def update
    tour = Tour.find(params[:id])
    tour.update(update_params)
    
    redirect_to tour_path(tour)
  end

  def destroy
    tour = Tour.find(params[:id])
    tour.destroy
    redirect_to tours_path
  end
  
  private
  def create_params
    params.require(:tour).permit(:title, :tag, :area, :address, :url, :text, images_attributes: [:img])
  end
  
  def update_params
    params.require(:tour).permit(:title, :tag, :area, :address, :url, :text)
  end
end
