class TagController < ApplicationController
  def show
    @tag = params[:format]
    @tour = Tour.where(tag: @tag).page(params[:page]).per(10).order(:id)
  end
end
